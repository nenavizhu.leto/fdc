import { authentication, createDirectus, rest, type DirectusClient, type RestClient } from "@directus/sdk";

const directus = createDirectus(import.meta.env.PUBLIC_DIRECTUS_URL).with(rest()).with(authentication())

export const getDirectusClient = async (): Promise<typeof directus>  => {
	const token = await directus.getToken()

	if (token) return directus;

	if (import.meta.env.DIRECTUS_EMAIL && import.meta.env.DIRECTUS_PASSWORD) {
		const auth_data = await directus.login(
			import.meta.env.DIRECTUS_EMAIL,
			import.meta.env.DIRECTUS_PASSWORD
		);
		return directus;
	} 
}
